package bearman;

import food.Food;

public class PizzaEatingBearman extends Bearman{
	
	public void eat(Food yum) {
		yum.feedBearman(this);
	}
}
