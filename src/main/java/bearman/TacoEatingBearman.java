package bearman;

import food.Food;

public class TacoEatingBearman extends Bearman{

	public void eat(Food yum) {
		yum.feedBearman(this);
	}
	
}
