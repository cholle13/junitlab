package food;

import bearman.TacoEatingBearman;

public class Taco extends Food {
	
	/**
	 * Taco satisfies the taco eating bearman
	 * 
	 * @param tacoEater
	 *            The TacoEatingBearman to feed
	 */
	@Override
	public void feedBearman(TacoEatingBearman tacoEater) {
		tacoEater.setHungry(false);
	}
}
